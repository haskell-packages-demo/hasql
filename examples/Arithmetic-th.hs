-- import Data.Functor.Contravariant ( (>$<) )

import Data.Int ( Int64 )

import Hasql.Connection qualified as Connection
    ( acquire
    , settings
    )
-- import Hasql.Decoders qualified as Decoders
    -- ( column
    -- , int8
    -- , nonNullable
    -- , singleRow
    -- )
-- import Hasql.Encoders qualified as Encoders
    -- ( int8
    -- , nonNullable
    -- , param
    -- )
import Hasql.Session ( Session )
import Hasql.Session qualified as Session ( run, statement )

import Hasql.Statement ( Statement
        -- ( Statement )
    )

import Hasql.TH qualified as TH ( singletonStatement )
-- from "hasql-th"


import Prelude
    ( Either( Right )
    , IO
    , print
    )
    -- (<>), (<$>), (<*>)
    -- , Bool( True )
    -- , fst
    -- , snd


main :: IO ()
main = do
  Right connection <- Connection.acquire connectionSettings
  result <- Session.run (sumAndDivModSession 3 8 3) connection
  print result
  where
    connectionSettings = Connection.settings "localhost" 5432 "postgres" "" "postgres"


-- * Sessions
-- 
-- Session is an abstraction over the database connection and all possible errors.
-- It is used to execute statements.
-- It is composable and has a Monad instance.
-- 
-- It's recommended to define sessions in a dedicated 'Sessions'
-- submodule of your project.
-------------------------

sumAndDivModSession :: Int64 -> Int64 -> Int64 -> Session (Int64, Int64)
sumAndDivModSession a b c = do
  -- Get the sum of a and b
  sumOfAAndB <- Session.statement (a, b) sumStatement
  -- Divide the sum by c and get the modulo as well
  Session.statement (sumOfAAndB, c) divModStatement


-- * Statements
-- 
-- Statement is a definition of an individual SQL-statement,
-- accompanied by a specification of how to encode its parameters and
-- decode its result.
-- 
-- It's recommended to define statements in a dedicated 'Statements'
-- submodule of your project.
-------------------------

sumStatement :: Statement (Int64, Int64) Int64
sumStatement =
  [TH.singletonStatement|
    select ($1 :: int8 + $2 :: int8) :: int8
    |]

divModStatement :: Statement (Int64, Int64) (Int64, Int64)
divModStatement =
  [TH.singletonStatement|
    select
      (($1 :: int8) / ($2 :: int8)) :: int8,
      (($1 :: int8) % ($2 :: int8)) :: int8
    |]
