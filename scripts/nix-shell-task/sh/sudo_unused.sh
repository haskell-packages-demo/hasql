#! /usr/bin/env nix-shell
#! nix-shell -i sh

. scripts/entrypoint/sh/header.sh

exec env \
    HOME=/home/user \
    LOGNAME=user \
    USER=user \
    SUDO=sudo \
    setpriv \
        --reuid=$(id -u user) \
        --regid=$(id -g user) \
        --init-groups \
        -- \
        "$@"
