#! /bin/sh
set -o errexit -o nounset -o noglob
# Not in dash: -o pipefail
set -o xtrace

whoami
nix-channel --version
nix-shell --version
nix-channel \
    --add https://nixos.org/channels/$1 \
        nixpkgs
nix-channel --update
nix-shell \
        --packages cabal2nix \
        --run "cabal2nix ./." \
        --quiet \
    > examples.nix
