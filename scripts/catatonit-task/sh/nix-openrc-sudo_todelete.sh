cd $CI_PROJECT_DIR
. scripts/entrypoint/sh/header.sh

openrc sysinit
openrc boot
openrc default

export NIX_REMOTE=daemon
nix-channel --version
nix-channel --add $NIXPKGS_URL nixpkgs
nix-channel --update
nix-shell --version
nix-shell \
        --packages cabal2nix \
        --run "cabal2nix ./." \
        --quiet \
    > examples.nix

chmod u+x scripts/nix-shell-task/sh/sudo.sh
nix_shell_run () { 
    nix-shell --run "$@"
}
# scripts/nix-shell-task/sh/sudo.sh "$@"
nix-shell --quiet --no-build-output  --run sh

x=$?
openrc shutdown
exit $x

#^ Trap is not working (from gitlab-ci.yml), maybe it would wor^ with bash.
#^ ["/bin/sh", "-c", "trap 'openrc shutdown' 0 ; openrc ; su user \\\"\\\$@\\\"", "--"]
#^ SUDO=sudo exec time su user \\"\\$@\\" ; x=$?
