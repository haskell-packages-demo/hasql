cd $CI_PROJECT_DIR
. scripts/entrypoint/sh/header.sh

touch /run/nix-daemon.pid
start-stop-daemon \
        --verbose \
        --progress \
        --background \
        --pidfile /run/nix-daemon.pid \
        --make-pidfile \
        --wait 1000 \
        -- \
    /usr/sbin/nix-daemon
nix-channel --add $NIXPKGS_URL nixpkgs
nix-channel --update
nix-shell \
        --packages cabal2nix \
        --run "cabal2nix ./." \
    > examples.nix
exec nix-shell --run sh

#  --packages
# cabal-install
# "haskellPackages.ghcWithPackages (pkgs: [
# pkgs.hasql
# pkgs.hasql-th
# ])"


