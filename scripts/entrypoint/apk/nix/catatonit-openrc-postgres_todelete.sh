. $CI_PROJECT_DIR/scripts/entrypoint/sh/header.sh

if ! id -u user &> /dev/null ; then
    apk add \
            --no-cache \
            --quiet \
        catatonit \
        openrc \
        postgresql \
        psmisc \
        setpriv \
        sudo \
        wget \
    ;
    apk add \
            --quiet \
            --no-cache \
            --repository 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' \
        nix \
        nix-openrc \
    ;
    adduser -D user
    echo "user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/user
    rc-update add nix-daemon default
    # rc-update add postgresql default
    fi

exec catatonit -- sh $CI_PROJECT_DIR/scripts/catatonit-task/sh/nix-openrc-sudo.sh "$@"

