. $CI_PROJECT_DIR/scripts/entrypoint/sh/header.sh

apk add \
        --no-cache \
        --quiet \
    catatonit \
    openrc \
    postgresql \
    psmisc \
    wget \
;
apk add \
        --quiet \
        --no-cache \
        --repository 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' \
    nix \
;
exec catatonit -- sh $CI_PROJECT_DIR/scripts/catatonit-task/sh/nix-postgres_SSD.sh "$@"

