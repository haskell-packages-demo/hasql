cd $CI_PROJECT_DIR
set -o errexit -o nounset -o noglob
# Not in dash: -o pipefail
(. /etc/os-release ; echo $PRETTY_NAME )
set -o xtrace

export USER=nixbld


if ! id -g nixbld &> /dev/null ; then
    ${SUDO:-} apt-get update --quiet=2
    ${SUDO:-} apt-get install \
            --no-install-suggests \
            --no-install-recommends \
            --quiet=2 \
        apt-utils \
        moreutils \
        > /dev/null
    CHRONIC=chronic
    ${CHRONIC:-} ${SUDO:-} apt-get install \
            --no-install-suggests \
            --no-install-recommends \
            --quiet=2 \
        ca-certificates \
        catatonit \
        nix-bin \
        sudo \
    ;
        # postgresql \
        # psmisc \
    whoami
    groups
    cat /etc/default/useradd || true
    # addgroup nixbld
    # adduser nixbld nixbld
    # useradd \
    #         --no-create-home \
    #         --system \
    #         --user-group \
    #     nixbld
    ls --color /home || true
    chmod a+x scripts/group_user/group_useradd.sh
    scripts/group_user/group_useradd.sh \
        $USER
    echo "nixbld ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER
        # --home /var/empty \
        # -M \
    # useradd \
    #     --comment "Nix build user" \
    #     --create-home \
    #     --gid nixbld \
    #     --groups nixbld \
    #     --no-user-group \
    #     --system \
    #     --shell /bin/sh \
    #     nixbld
    groups $USER
    # mkdir /home/nixbld
    # chown nixbld:nixbld /home/nixbld
    ls -l --color /home || true
    ls -dl --color /home/$USER || true
    # mkdir /nix
    # chown nixbld:nixbld /nix
    # ls -l --color /nix
    # mkdir /nix/var
    # chown nixbld:nixbld /nix/var
    # ls -l --color /nix/var
    # mkdir /nix/var/nix
    # chown nixbld:nixbld /nix/var/nix
    ls -dl --color /nix
    ls -dl --color /nix/var
    ls -dl --color /nix/var/nix
    # adduser -D user
    # rc-update add postgresql default
    fi

chmod a+x scripts/cabal2nix/cabal2nix.sh
chmod a+x nix-shell-task-trivial.sh

export \
        HOME=/home/$USER \
        LANG="C.utf8" \
        LANGUAGE="C.utf8" \
        LC_ALL="C.utf8" \
        LOGNAME=nixbld \
        SUDO=sudo

setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    ./scripts/cabal2nix/cabal2nix.sh \
        $NIXOS_CHANNEL

exec catatonit -- \
    setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    ./nix-shell-task-trivial.sh "$@"


# export NIX_REMOTE=daemon
setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    whoami
# ls -l --color /root/.nix-defexpr || true
# su nixbld -- $(command -v nix-channel) --version || true
HOME=/home/nixbld setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    nix-channel --version
HOME=/home/nixbld setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    nix-shell --version
HOME=/home/nixbld setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    nix-channel \
            --add $NIXPKGS_URL \
        nixpkgs
HOME=/home/nixbld setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    nix-channel --update

HOME=/home/nixbld setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    nix-shell \
        --packages cabal2nix \
        --run "cabal2nix ./." \
        --quiet \
    > examples.nix

locale -a
printenv LANGUAGE || true
printenv LC_ALL || true
printenv LANG || true
chmod a+x nix-shell-task-trivial.sh
exec catatonit -- \
    env \
        HOME=/home/nixbld \
        LANG="C.utf8" \
        LANGUAGE="C.utf8" \
        LC_ALL="C.utf8" \
        LOGNAME=nixbld \
        USER=nixbld \
        SUDO=sudo \
    setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    ./nix-shell-task-trivial.sh "$@"
    # php --run 'echo "\"/usr/bin/env ".implode(" ", array_slice($argv, 1))."\"" ; pcntl_exec("/usr/bin/nix-shell", array_merge(["--quiet", "--no-build-output", "--run"], ["\"/usr/bin/env ".implode(" ", array_slice($argv, 1))."\""]), getenv());' "$@"
    # nix-shell \
        # --quiet \
        # --no-build-output \
        # --run "env $@"

        # --run \'"$@"\'

        # --run \'sh ${@:+-c \"$@\"}\'

    # php --run 'pcntl_exec("/usr/bin/nix-shell", array_merge(["--quiet", "--no-build-output", "--run"], ["\"".implode(" ", array_slice($argv, 1))."\""]), getenv());' "$@"
