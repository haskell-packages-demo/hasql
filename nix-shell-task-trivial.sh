#! /usr/bin/env nix-shell
#! nix-shell --quiet --quiet -i /bin/sh

set -o errexit -o nounset -o noglob
# Not in dash: -o pipefail
set -o xtrace

command -v /bin/sh

${CHRONIC:-} ${SUDO:-} apt-get install \
        --no-install-suggests \
        --no-install-recommends \
        --quiet=2 \
    psmisc \
;
pstree --unicode

exec env "$@"
